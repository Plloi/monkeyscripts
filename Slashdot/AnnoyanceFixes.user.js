// ==UserScript==
// @name Slashdot Annoyance Fixes
// @namespace http://javex.tech/
// @version 0.0.1
// @description  fixes some minor issues with my slashdot viewing experience
// @author       Shaun Hammill <plloi.pllex@gmail.com>
// @match https://*.slashdot.org/*
// @grant none
// ==/UserScript==

// Text encoding is a pain, gimme apostrophes please.
$(".commentBody>div>p").map((_,elm)=>elm.innerText=elm.innerText.replace(/â\(TM\)/g,"'"))
