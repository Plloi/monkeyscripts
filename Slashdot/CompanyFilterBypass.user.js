// ==UserScript==
// @name Slashdot Company Filter Bypass
// @namespace http://javex.tech/
// @version 0.0.4
// @description  Relink games.slashdot.org/<article> to slashdot.org/<article> because company filter blocks urls with games in the domain
// @author       Shaun Hammill <plloi.pllex@gmail.com>
// @match https://slashdot.org/*
// @match https://reader.miniflux.app/*
// @grant none
// ==/UserScript==

Array.from(document.querySelectorAll("a")).map((elm) => elm.href = elm.href.replace("games.slashdot.org","slashdot.org"))
